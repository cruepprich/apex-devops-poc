#!/bin/bash
echo Exporting application 106
~/oracle/sqlcl/bin/sql -S /nolog @ssh_tunnel.sql <<EOF
@timer_start
set serveroutput on
--cd ~/Downloads
spool f106_apex_export.sql
apex export 106
spool off
@timer_stop
exit
EOF