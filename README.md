# APEX DevOps POC

## Installation
Navigate to schema directory:

$ ~/oracle/sqlcl/bin/sql /nolog @ssh_tunnel.sql

SQL> @install


## Uninstall
Navigate to schema directory:

$ ~/oracle/sqlcl/bin/sql /nolog @ssh_tunnel.sql

SQL> @uninstall
