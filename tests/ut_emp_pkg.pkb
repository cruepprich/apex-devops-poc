create or replace package body ut_emp_pkg as

  procedure give_raise is
    l_empno constant number := 7788;
    l_amount constant number := 9999;
  begin
    emp_pkg.give_raise(p_empno => l_empno, p_amount => l_amount); 
    ut.expect( emp_pkg.get_sal(p_empno => l_empno) ).to_equal(l_amount);
  end;

  procedure get_sal is
    l_empno constant number := 7788;
    l_sal number;
  begin
    select sal into l_sal
      from emp
     where empno = l_empno;
     
    ut.expect( emp_pkg.get_sal(p_empno => l_empno) ).to_equal(l_sal);
  end;
end;