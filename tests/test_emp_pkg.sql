set serveroutput on
declare
  l_prc           constant  varchar2(30) := 'EMP_PKG.GIVE_RAISE';
  l_test_run_id   constant  number := 1;
  l_empno                   number := 7788;
  l_sal                     number;
  l_newsal                  number := 9999;
  l_result                  test_results.test_result%type;
  l_err                     test_results.test_result%type;
begin
  dbms_output.put_line('Testing give_raise.');
  begin --give_raise
    emp_pkg.give_raise(p_empno => l_empno, p_amount => l_newsal);

    select sal into l_sal
        from emp 
    where empno = l_empno;

    if l_sal = l_newsal then
        l_result := 'PASS';
    else
        l_result := 'FAIL';
    end if;

    insert into test_results (test_run_id, program_name, test_result)
    values (l_test_run_id, l_prc,l_result);
    exception
        when others then
            l_err := sqlerrm;
            insert into test_results (test_run_id, program_name, test_result)
            values (l_test_run_id, l_prc, l_err);
  end; --give_raise




  dbms_output.put_line('Testing get_sal.');
  begin --get_sal
    select sal into l_sal
        from emp 
    where empno = l_empno;

    l_newsal := emp_pkg.get_sal(p_empno => l_empno);


    if l_sal = l_newsal then
        l_result := 'PASS';
    else
        l_result := 'FAIL';
    end if;

    insert into test_results (test_run_id, program_name, test_result)
    values (l_test_run_id, l_prc,l_result);
    exception
        when others then
            l_err := sqlerrm;
            insert into test_results (test_run_id, program_name, test_result)
            values (l_test_run_id, l_prc,l_err);
  end; --get_sal


end;
/