@test_emp_pkg.sql

PROMPT =======================================================
PROMPT Failed tests:
select program_name, test_result
  from test_results
 where test_result != 'PASS';
PROMPT =======================================================

-- repeat for log file
set termout off echo off heading off feedback off
spool fail_count.txt
select count(*)
  from test_results
 where test_result != 'PASS';
spool off
