create table test_results (
    id               number generated always as identity,
    test_run_id      number not null,
    test_date        date default sysdate,
    program_name     varchar2(30) not null,
    test_result      varchar2(200) not null
);

