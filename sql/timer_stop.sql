exec :timer := (dbms_utility.get_time - :timer)/100
set serveroutput on
exec dbms_output.put_line('Duration: '||round(:timer,0)||' seconds.')