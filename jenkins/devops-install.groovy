#!groovy

def username       = "devops"
def password       = "devops"
def sqlcl          = "~/oracle/sqlcl/bin/sql"
def fails

node {
    stage ("Print Variables") {
        echo "username ${username}"
        echo "password ${password}"
        echo "sqlcl ${sqlcl}"
    }

    stage ("Pull repository") {
        git 'https://bitbucket.org/cruepprich/apex-devops-poc.git'
        echo "Git pulled"
    }

    stage ("Uninstall objects") {
        dir ("test_objects") {
            sh "sh uninstall.sh"
        }
        
        dir ("schema") {
            sh "sh uninstall.sh"
        }
        

    }

    stage ("Install objects") {
        dir ("test_objects") {
            sh "sh install.sh"
        }
        dir ("schema") {
            sh "sh install.sh"
        }

        dir ("apex") {
            sh "sh import.sh"
        }

    }

    stage ("Install APEX") {
        dir ("apex"){
            sh "sh import.sh"
        }
    }

    stage ("UT Test") {
        dir ("tests") {
            sh "sh run_ut.sh"
            sh "touch *.xml"
            junit "results.xml"
        }
    }

    // stage ("Tests") {
    //     dir ("tests") {
    //         //Run SQL tests
    //         sh "./run_tests.sh"

    //         //Remove empty lines and padding
    //         sh "./trimFile.sh"
    //         fails = readFile 'fail_count.txt'
    //     }
        
    //     // Cast string to integer
    //     nbrFails = fails.toInteger()
        
    //     if (nbrFails == 0) {
    //         echo "SUCCESS"
    //         currentBuild.result = 'SUCCESS'
    //     } else {
    //         echo "FAILURE"
    //         echo "There are $fails failures."
    //         currentBuild.result = 'FAILURE'
    //     }
    // }


}