#!groovy

def username       = "devops"
def password       = "devops"
def sqlcl          = "~/oracle/sqlcl/bin/sql"
def fails

node {
    stage ("Pull repository") {
        git 'https://bitbucket.org/cruepprich/apex-devops-poc.git'
        echo "Git pulled"
    }

    stage ("Build & test") {
        sh "./install.sh"
    }


    stage ("Get Results") {
        dir ("tests") {

            //Remove empty lines and padding
            sh "./trimFile.sh"
            fails = readFile 'fail_count.txt'
        }
        
        // Cast string to integer
        nbrFails = fails.toInteger()
        
        if (nbrFails == 0) {
            echo "SUCCESS"
            currentBuild.result = 'SUCCESS'
        } else {
            echo "FAILURE"
            currentBuild.result = 'FAILURE'
        }
    }


}