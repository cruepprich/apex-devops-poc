create or replace package body emp_pkg
is 
procedure give_raise(
      p_empno in number,
      p_amount in number
)
is 
begin 
    update emp 
       set sal = p_amount
     where empno = p_empno;

    commit;
end give_raise;

 
function get_sal(
      p_empno in number
) return number
is 
  l_sal number;
begin
  select sal into l_sal
    from emp
   where empno = p_empno;

  return l_sal;
end get_sal;

end emp_pkg;
/
show err

