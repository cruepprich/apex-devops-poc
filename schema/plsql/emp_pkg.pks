create or replace package emp_pkg
is 
procedure give_raise(
      p_empno in number,
      p_amount in number
);

 
function get_sal(
      p_empno in number
) return number;

end emp_pkg;
/
show err