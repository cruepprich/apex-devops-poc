drop table emp;
drop table dept;

drop sequence emp_seq;
drop sequence dept_seq;

drop package emp_pkg;