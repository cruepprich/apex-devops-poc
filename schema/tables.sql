--------------------------------------------------------
--  DDL for Table DEPT
--------------------------------------------------------
CREATE TABLE DEPT
       (DEPTNO NUMBER(2) CONSTRAINT PK_DEPT PRIMARY KEY,
        DNAME VARCHAR2(14) ,
        LOC VARCHAR2(13) 
) ;


CREATE SEQUENCE DEPT_SEQ;

CREATE OR REPLACE EDITIONABLE TRIGGER DEPT_TRG1 
              before insert on dept
              for each row
              begin
                  if :new.deptno is null then
                      select dept_seq.nextval into :new.deptno from sys.dual;
                 end if;
              end;
/
ALTER TRIGGER DEPT_TRG1 ENABLE;




--------------------------------------------------------
--  DDL for Table EMP
--------------------------------------------------------

CREATE TABLE EMP
       (EMPNO NUMBER(4) CONSTRAINT PK_EMP PRIMARY KEY,
        ENAME VARCHAR2(10),
        JOB VARCHAR2(9),
        MGR NUMBER(4),
        HIREDATE DATE,
        SAL NUMBER(7,2),
        COMM NUMBER(7,2),
        DEPTNO NUMBER(2) CONSTRAINT FK_DEPTNO REFERENCES DEPT
);


CREATE SEQUENCE EMP_SEQ;

CREATE OR REPLACE EDITIONABLE TRIGGER EMP_TRG1 
              before insert on emp
              for each row
              begin
                  if :new.empno is null then
                      select emp_seq.nextval into :new.empno from sys.dual;
                 end if;
              end;
/
ALTER TRIGGER EMP_TRG1 ENABLE;

