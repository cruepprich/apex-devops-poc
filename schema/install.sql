@tables.sql
@seed_data.sql
@plsql/emp_pkg.pks
@plsql/emp_pkg.pkb

PROMPT ============================
PROMPT Invalid objects:
set sqlformat ansiconsole

select object_type, object_name
  from user_objects
 where status != 'VALID';
PROMPT ============================